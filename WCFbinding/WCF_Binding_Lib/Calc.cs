﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Description;
using System.Runtime.Serialization;

namespace WCF_Binding_Lib
{
    [DataContract]
    public class Calc
    {
        [DataMember]
        public double n1;
        [DataMember]
        public double n2;
    }
}
