﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFbindingClient
{
    class Program
    {
        public const String Host = "net.tcp://localhost";
        public const Int32 Port = 31227;
        public const String ServiceName = "MathService";

        private delegate double CalcFunction(double a, double b);

        static void Main(string[] args)
        {
            // Address
            string svcAddress = Host + ":" + Port + "/" + ServiceName;

            // Binding
            NetTcpBinding tcpb = new NetTcpBinding(SecurityMode.Message);
            ChannelFactory<ICalcService> chFactory = new ChannelFactory<ICalcService>(tcpb);

            // Endpoint
            EndpointAddress epAddress = new EndpointAddress(svcAddress);

            // Create Channel
            ICalcService calcService = chFactory.CreateChannel(epAddress);

            // Print Header
            PrintHeader();

            // Declare attributes
            CalcFunction calcFunction;
            string calcOperator;

            while (true)
            {
                // Let the User select a operation 
                string operationSelection = "0";
                do
                {
                    Console.WriteLine("\nBitte wähle eine Rechenoperation:");
                    Console.WriteLine("1 - Addition");
                    Console.WriteLine("2 - Substraktion");
                    Console.WriteLine("3 - Multiplikation");
                    Console.WriteLine("4 - Divsion");
                    Console.WriteLine("");
                    Console.WriteLine("5- Beenden");
                    Console.WriteLine("Auswahl: ");
                    operationSelection = Console.ReadLine();

                } while (operationSelection != "1" && operationSelection != "2" &&
                         operationSelection != "3" && operationSelection != "4" &&
                         operationSelection != "5");
                
                switch (operationSelection)
                {
                    case "1":
                        calcFunction = (a, b) => { return calcService.Add(a, b); };
                        calcOperator = "+";
                        break;
                    case "2":
                        calcFunction = (a, b) => { return calcService.Substract(a, b); };
                        calcOperator = "-";
                        break;
                    case "3":
                        calcFunction = (a, b) => { return calcService.Multiply(a, b); };
                        calcOperator = "*";
                        break;
                    case "4":
                        calcFunction = (a, b) => { return calcService.Divide(a, b); };
                        calcOperator = "/";
                        break;
                    case "5":
                        PrintFooter();
                        chFactory.Close();
                        calcFunction = (a, b) => { return 0; }; // Muss man deklarieren???
                        calcOperator = "";
                        Environment.Exit(0);
                        break;
                    default:
                        calcFunction = (a, b) => { return 0; }; // Muss man deklarieren???
                        calcOperator = "";
                        break;
                }

                // Calculation
                do
                {
                    try
                    {
                        // Ask to enter two Numbers
                        Console.WriteLine("Bitte gebe die erste Zahl ein");
                        double a = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Bitte gebe die zweite Zahl ein");
                        double b = Convert.ToDouble(Console.ReadLine());

                        // Calcaulate result
                        Console.WriteLine("Verbunden zu Service '" + svcAddress + "'.");

                        double result = calcFunction(a, b);

                        Console.WriteLine("--> Das Ergbnis von " + a.ToString() + calcOperator + b.ToString() +  
                                            " ist " + result + ".");

                        // Console.WriteLine("Press key to quit.");
                        // Console.ReadKey();


                        break;
                    }
                    catch
                    {
                        Console.WriteLine("Das hat nicht funktioniert!");
                        break;
                    }
                } while (true);

            }

        }

        private static void PrintHeader()
        {
            Console.WriteLine("##############################################");
            Console.WriteLine("#                                            #");
            Console.WriteLine("#              EASY CALCULATOR               #");
            Console.WriteLine("#                     by                     #");
            Console.WriteLine("#        Sofya, Steve, Pascal, Otto          #");
            Console.WriteLine("#                                            #");
            Console.WriteLine("##############################################\n");
        }

        private static void PrintFooter()
        {
            Console.WriteLine("\n##############################################");
            Console.WriteLine("#                                            #");
            Console.WriteLine("#       Vielen Dank fuer die Benutzung       #");
            Console.WriteLine("#                                            #");
            Console.WriteLine("##############################################");
        }
    }
}
