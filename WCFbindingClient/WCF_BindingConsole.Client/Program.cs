﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace WCF_BindingConsole.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            //creating the object of WCF service client         
            ServiceReference1.CalcServiceClient calc = new ServiceReference1.CalcServiceClient();
            Console.WriteLine("Bitte gebe die erste Zahl ein");
            Int32 a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Bitte gebe die zweite Zahl ein");
            Int32 b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Das Erebnis der Addtion ist");
            Console.WriteLine(calc.Add(a, b));
            Console.ReadKey();
        }
    }
}
